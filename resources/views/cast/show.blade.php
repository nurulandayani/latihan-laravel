@extends('layout.master')

@push('nav')
@include('partial.nav2');
@endpush

@push('sidebar')
@include('partial.sidebar');
@endpush

@section('title')
Show Cast
@endsection

@push('title')
Show Cast
@endpush

@section('content')
<h2>Show Cast : {{$cast->id}}</h2>
<h4>{{$cast->nama}}</h4>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-primary my-2">Kembali ke List</a>

@endsection