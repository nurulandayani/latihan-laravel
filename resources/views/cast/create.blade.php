@extends('layout.master')

@push('nav')
@include('partial.nav2');
@endpush

@push('sidebar')
@include('partial.sidebar');
@endpush

@section('title')
Tambah Data Cast
@endsection

@section('content')

<div>
    <h2>Tambah Data</h2>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur</label>
            <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Biografi</label>
            <textarea class="form-control" rows="3" name="biografi" id="biografi" placeholder="Masukkan biografi"></textarea>
            @error('biografi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection