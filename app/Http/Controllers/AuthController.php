<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('resources.register');
    }

    public function submit(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('resources.welcome', compact('fname', 'lname'));
    }
}
